const io = require("socket.io-client");
const adminSocket = io.connect("http://localhost:5000/admin");

adminSocket.on("news", (data) => {
  console.log(data);
});

adminSocket.on("update", (data) => {
  console.log(data);
});

adminSocket.on("private", (data) => {
  console.log("private message: " + data);
});
