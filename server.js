const express = require("express");
const app = express();
const chalk = require("chalk");
const server = require("http").Server(app);
const io = require("socket.io")(server);
const PORT = process.env.PORT || 5000;

var admin = io.of("/admin");
var user = io.of("/user");

app.get("/", (req, res) => {
  res.send("Turnout Socket API");
});

// Send message to a namespace 'admin' or 'user'
app.get("/sendtons", (req, res) => {
  let ns = req.query.ns;
  let msg = req.query.msg;

  switch (ns) {
    case "admin":
      admin.emit("news", msg);
      break;
    case "user":
      user.emit("news", msg);
      break;
  }
  res.send(`Sent message to namespace ${ns}`);
});

// Send private message to a particular message to 'admin' or 'user'
app.get("/sendto", (req, res) => {
  let id = req.query.id;
  let msg = req.query.msg;
  let ns = req.query.ns;

  switch (ns) {
    case "admin":
      admin.to(`admin${id}`).emit("private", msg);
      break;
    case "user":
      user.to(`user${id}`).emit("private", msg);
      break;
  }
  res.send(`Sent private message to ${ns}${id}`);
});

server.listen(PORT, () => {
  console.log(chalk.white.bgYellow.bold(`Running at ${PORT}`));
});

var admins = [];
var users = [];

admin.on("connection", (socket) => {
  socket.join(`admin${admins.length}`);

  socket.emit("news", "Hi admin client");

  socket.on("disconnect", () => {
    admins = admins.filter((currSocket) => currSocket !== socket.id);
    console.log(
      chalk.red("Disconnected a admin: ") +
        chalk.white.bgGray(socket.id) +
        "\n" +
        chalk.yellow("Remaining admins: ") +
        chalk.white.bgGray(admins.length)
    );
  });

  console.log(
    chalk.green("Connected to new admin: ") + chalk.white.bgGrey(socket.id)
  );
  admins.push(socket.id);
});

user.on("connection", (socket) => {
  socket.join(`user${users.length}`);

  socket.emit("news", "Hi user client");

  socket.on("update", (user, lat, lng) => {
    if (!users.includes(user)) {
      socket.nickname = user;
    }
    console.log(`Location update ${lat}:${lng} from ${user}`);
    admin.emit("update", user, lat, lng);
  });

  socket.on("disconnect", () => {
    users = users.filter((currSocket) => currSocket !== socket.id);
    console.log(
      chalk.red("Disconnected a user: ") +
        chalk.white.bgGray(socket.id) +
        "\n" +
        chalk.yellow("Remaining users: ") +
        chalk.white.bgGray(users.length)
    );
    if (!socket.nickname) return;
    admin.emit("unhook", socket.nickname);
  });

  console.log(
    chalk.green("Connected to new user: ") + chalk.white.bgGrey(socket.id)
  );
  users.push(socket.id);
});
