const io = require("socket.io-client");
const userSocket = io.connect("http://localhost:5000/user");

userSocket.on("news", (data) => {
  console.log(data);
});

userSocket.on("update", (data) => {
  console.log(data);
});

userSocket.on("private", (data) => {
  console.log("private message: " + data);
});
